﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PracticiensController : ControllerBase
    {
        private readonly ProjectContext _context;

        public PracticiensController(ProjectContext context)
        {
            _context = context;
        }

        // GET: api/Practiciens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Practicien>>> GetPracticienItems()
        {
            return await _context.PracticienItems.ToListAsync();
        }

        // GET: api/Practiciens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Practicien>> GetPracticien(int id)
        {
            var practicien = await _context.PracticienItems.FindAsync(id);

            if (practicien == null)
            {
                return NotFound();
            }

            return practicien;
        }

        // PUT: api/Practiciens/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPracticien(int id, Practicien practicien)
        {
            if (id != practicien.id)
            {
                return BadRequest();
            }

            _context.Entry(practicien).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PracticienExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Practiciens
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Practicien>> PostPracticien(Practicien practicien)
        {
            _context.PracticienItems.Add(practicien);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPracticien", new { id = practicien.id }, practicien);
        }

        // DELETE: api/Practiciens/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Practicien>> DeletePracticien(int id)
        {
            var practicien = await _context.PracticienItems.FindAsync(id);
            if (practicien == null)
            {
                return NotFound();
            }

            _context.PracticienItems.Remove(practicien);
            await _context.SaveChangesAsync();

            return practicien;
        }

        private bool PracticienExists(int id)
        {
            return _context.PracticienItems.Any(e => e.id == id);
        }
    }
}
