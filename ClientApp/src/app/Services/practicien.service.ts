import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root',
})
export class PracticienService {
  myAppUrl = "";

  constructor(public _http: Http, @Inject('BASE_URL') baseUrl: string) {
    this.myAppUrl = baseUrl;
  }

  getAllPracticiens() {
    return this._http.get(this.myAppUrl + 'api/Practiciens')
      .pipe(map(res => res.json()))
  }

  savePracticiens(practicien) {
    return this._http.post(this.myAppUrl + 'api/Practiciens', practicien)
      .pipe(map(res => res.json()))
  };
}
