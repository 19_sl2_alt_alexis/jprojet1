import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PracticienService } from '../Services/practicien.service';
import { PracticienDetailComponent } from './practicien-detail/practicien-detail.component';
import { NgForm, FormBuilder, FormGroup, Validator, FormControl, Validators } from '@angular/forms';
import { error } from '@angular/compiler/src/util';
import { from } from 'rxjs';

@Component({
  selector: 'app-practicien',
  templateUrl: './practicien.component.html',
  styleUrls: ['./practicien.component.css']
})
export class PracticienComponent implements OnInit {

  private listPracticiens: Practicien[];

  constructor(private http: HttpClient, private practicienService: PracticienService, private _fb: FormBuilder, private dialog: MatDialog) { }

  ngOnInit() {

    this.practicienService.getAllPracticiens().subscribe(data => {
      this.listPracticiens = data;
    });

  }

  openDialog(element: Practicien): void {

    const dialogRef = this.dialog.open(PracticienDetailComponent, {
      data: {
        element
      },
      width: '800px',
      height: '700px',
      autoFocus: true
    });

    dialogRef.afterClosed().subscribe((practicien: Practicien) => {
      this.updatePracticiens();
    })

  }

  updatePracticiens() {
    this.practicienService.getAllPracticiens().subscribe(data => {
      this.listPracticiens = data;
    })
  }
}

interface Practicien {
  id: number;
  nom: string;
  adresse: string;
  latitude: number;
  longitude: number;
}
