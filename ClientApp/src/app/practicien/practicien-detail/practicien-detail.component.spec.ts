import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticienDetailComponent } from './practicien-detail.component';

describe('PracticienDetailComponent', () => {
  let component: PracticienDetailComponent;
  let fixture: ComponentFixture<PracticienDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PracticienDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticienDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
