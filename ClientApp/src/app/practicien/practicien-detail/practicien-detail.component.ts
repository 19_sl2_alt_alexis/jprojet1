import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as L from 'leaflet';
import { from } from 'rxjs';

@Component({
  selector: 'app-practicien-detail',
  templateUrl: './practicien-detail.component.html',
  styleUrls: ['./practicien-detail.component.css']
})
export class PracticienDetailComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<PracticienDetailComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private dialog: MatDialog) { }

  ngOnInit() {

    console.log(this.data);

  }

}
