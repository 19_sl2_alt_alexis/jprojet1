import { BrowserModule } from '@angular/platform-browser';
import { MatDialogModule } from '@angular/material/dialog';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { PracticienService } from './Services/practicien.service';
import { locationService } from './Services/location.service';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { MapComponent } from './map/map.component';
import { CommonModule } from '@angular/common';
import { MapDetailComponent } from './map/map-detail/map-detail.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { from } from 'rxjs';
import { PracticienComponent } from './practicien/practicien.component';
import { PracticienDetailComponent } from './practicien/practicien-detail/practicien-detail.component';
 
@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    MapComponent,
    MapDetailComponent,
    PracticienComponent,
    PracticienDetailComponent
  ],
  imports: [
    CommonModule,
    LeafletModule.forRoot(),
    MatDialogModule,
    HttpModule,
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'counter', component: CounterComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'map', component: MapComponent },
      { path: 'map-details', component: MapDetailComponent },
      { path: 'practicien', component: PracticienComponent },
      { path: 'practicien-detail', component: PracticienDetailComponent }
    ]),
    BrowserAnimationsModule
  ],
  providers: [PracticienService, locationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
