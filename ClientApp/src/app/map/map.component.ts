// Ajouter OnInit pour effectuer des opérations à l'initialisation du composant.
import { Component, OnInit, NgZone } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import * as L from 'leaflet';
import { Router, ActivatedRoute } from '@angular/router';
import { PracticienService } from '../Services/practicien.service';
import { locationService } from '../Services/location.service';
import { MapDetailComponent } from './map-detail/map-detail.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { NgForm, FormBuilder, FormGroup, Validator, FormControl, Validators } from '@angular/forms';
import { error } from '@angular/compiler/src/util';
import { from } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

// Implémenter OnInit
export class MapComponent implements OnInit {

  public PracticienList: Practicien[];
  practicienId: number;
  map: L.Map;
  icon: L.Icon;
  personalIcon: L.Icon;
  geolocForm: FormGroup;

  constructor(private http: HttpClient, private _router: Router, private _practicienService: PracticienService, private _locationService: locationService, private _fb: FormBuilder, private _avRoute: ActivatedRoute, private dialog: MatDialog, private ngZone: NgZone) { }

  // Fonction d'initialisation du composant.
  ngOnInit() {
    // Déclaration de la carte avec les coordonnées du centre et le niveau de zoom.

    this.geolocForm = new FormGroup({
      ville_geolocalisation: new FormControl("", [Validators.required]),
      adresse_geolocalisation: new FormControl("", [Validators.required]),
      code_postale_geolocalisation: new FormControl("", [Validators.required])
    })

    this.map = L.map('frugalmap').setView([50, 4], 8);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
      attribution: 'Frugal Map'
    }).addTo(this.map);

    this.icon = L.icon({
      iconUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.2.0/images/marker-icon.png'
    });
    this.personalIcon = L.icon({
      iconUrl: 'https://tombatossals.github.io/angular-leaflet-directive/bower_components/Leaflet.ExtraMarkers/src/images/markers_shadow.png'
    });

    this.refreshMarker();

    this.map.on("click", (e) => {
      this.ngZone.run(() => {
        this.openDialog(e);
      });
    });

  }

  getAllPracticiens() {
    this._practicienService.getAllPracticiens().subscribe(
      data => this.PracticienList = data
    )
  }

  changeViewSet(ville: string) {

  }

  geolocalisation() {
    if (navigator.geolocation) {
      this._locationService.getCurrentPosition().subscribe(res => {
        this.map.flyTo([res.coords.latitude, res.coords.longitude], 18);
        L.marker([res.coords.latitude, res.coords.longitude], { icon: this.personalIcon })
          .bindPopup("<p class='small'>Vous êtes ici</p>")
          .addTo(this.map).openPopup();
      });
    }
  }

  refreshMarker() {
    this._practicienService.getAllPracticiens().subscribe(
      data => data.forEach(practicien => {
        L.marker([practicien.latitude, practicien.longitude], { icon: this.icon })
          .bindPopup('<h4>' + practicien.nom + '</h4>' +
            '<p> Adresse: ' + practicien.adresse + '</p>'
          )
          .addTo(this.map)
      })
    );
  }

  openDialog(e): void {

    const dialogRef = this.dialog.open(MapDetailComponent, {
      data: {
        lat: e.latlng.lat,
        lng: e.latlng.lng
      },
      width: '800px',
      height: '700px',
      autoFocus: true
    });

    dialogRef.afterClosed().subscribe((practicien: Practicien) => {
      this.updatePracticiens();
    })

  }

  updatePracticiens() {
    this._practicienService.getAllPracticiens().subscribe(
      data => data.forEach(practicien => {
        L.marker([practicien.latitude, practicien.longitude], { icon: this.icon })
          .bindPopup('<h4>' + practicien.nom + '</h4>' +
            '<p> Adresse: ' + practicien.adresse + '</p>'
          )
          .addTo(this.map)
      })
    );
  }

  onSubmitGeoloc(value) {
    console.log(value);
    let query = "q=";
    if (value != null) {
      query += value.adresse_geolocalisation.replace(/ /g, '+');
      //query += "&" + value.ville_geolocalisation;
      query += "&postcode=" + value.code_postale_geolocalisation;
      query += "&limit=1";
    }
    this.http.get("https://api-adresse.data.gouv.fr/search/?" + query).subscribe((data: any) => {
      let tableau_coordonnes = data.features[0].geometry.coordinates;
      let longitude = tableau_coordonnes[0];
      let latitude = tableau_coordonnes[1];
      this.map.flyTo([latitude, longitude], 18);
        L.marker([latitude, longitude], { icon: this.personalIcon })
          .bindPopup("<p class='small'>Vous êtes ici</p>")
          .addTo(this.map).openPopup();
    })
    console.log(query);
  }
}

interface Practicien {
  id: number;
  nom: string;
  adresse: string;
  latitude: number;
  longitude: number;
}
