import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as L from 'leaflet';
import { from } from 'rxjs';


@Component({
  selector: 'app-map-detail',
  templateUrl: './map-detail.component.html',
  styleUrls: ['./map-detail.component.css']
})
export class MapDetailComponent implements OnInit {

  private detailPracticien;

  constructor(public dialogRef: MatDialogRef<MapDetailComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, private dialog: MatDialog) { }

  ngOnInit() {

    this.detailPracticien = new FormGroup({
      nom: new FormControl("", [Validators.required]),
      adresse: new FormControl("", [Validators.required]),
      longitude: new FormControl("", [Validators.required]),
      latitude: new FormControl("", [Validators.required])
    });

    this.http.get("https://nominatim.openstreetmap.org/reverse?format=json&lat=" + this.data.lat + "&lon=" + this.data.lng + "&zoom=18&addressdetails=1").
      subscribe((data: any) => {
        this.detailPracticien.controls["adresse"].setValue(data.display_name);
      })

    this.detailPracticien.controls["longitude"].setValue(this.data.lng);
    this.detailPracticien.controls["latitude"].setValue(this.data.lat);

  }

  onSubmitDetailPracticien(value) {
    let options = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    this.http.post(this.baseUrl + 'api/practiciens', value, options).subscribe(result => {
      this.dialogRef.close(result);
    })
  }

  

}
